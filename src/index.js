import React from 'react'
import ReactDOM, { render } from 'react-dom';
import {Main} from './containers/main/main.container'

ReactDOM.render(
    <Main/>, document.getElementById('root')
);
module.hot.accept();