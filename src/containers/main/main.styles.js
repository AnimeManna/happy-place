import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    toolbar: {
        minHeight: 128,
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(2),
      },
}))