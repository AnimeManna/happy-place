import React from 'react';
import {Header} from '../header/header.container'
import * as Styled from './main.styles'
import { ThemeProvider } from "@material-ui/styles"
import { CssBaseline } from "@material-ui/core"
import { createMuiTheme } from "@material-ui/core/styles"

const theme = createMuiTheme({
    palette: {
        primary: { 500: '#337ab7' }
    },
  })

export const Main = (props) => {
    const classes = Styled.useStyles(props)

    return <ThemeProvider theme={theme}>
        <CssBaseline />
    <div>
        <Header isAuth />
        <div className={classes.toolbar} />
        Hello world
    </div>
    </ThemeProvider> 
}