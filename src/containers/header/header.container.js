import React from 'react'
import { AppBar, Toolbar } from '@material-ui/core'
import { HeaderLogo } from "./header.styles"
import { Person } from '@material-ui/icons'
import { Icon } from "./components/header.components"

export const Header = (props) => {
    return <AppBar>
        <Toolbar>
            <HeaderLogo>
                Happy-place
            </HeaderLogo>
            <Icon>
                <Person />
            </Icon>
        </Toolbar>
    </AppBar>
}
