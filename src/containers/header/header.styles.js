import { styled } from "@material-ui/styles"


export const HeaderLogo = styled("div")({
    fontSize: "24px",
    flexGrow: 1,
})