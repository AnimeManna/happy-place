import React from "react"
import { HeaderIcon } from "./header.styles"

export const Icon = (props) => {
    return <HeaderIcon>
        {props.children}
    </HeaderIcon>
}