import { styled } from "@material-ui/styles"
import { IconButton } from "@material-ui/core"

export const HeaderIcon = styled(IconButton)({
    color: "white",
})